FROM ubuntu:18.04

ADD https://crowdstrike-installer-newest.s3-eu-west-1.amazonaws.com/falcon-sensor_5.18.0-8202_amd64.deb /tmp/

RUN apt-get -y update \
  && apt --fix-broken install /tmp/falcon-sensor_5.18.0-8202_amd64.deb -y \
  && rm -rf /var/lib/apt/lists/* /tmp/falcon-sensor_5.18.0-8202_amd64.deb

ADD sensor.sh /opt/sensor.sh
RUN chmod +x /opt/sensor.sh

CMD ["/opt/sensor.sh"]
