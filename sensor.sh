#!/bin/bash

/opt/CrowdStrike/falconctl -s -f --cid=${CID}
/etc/init.d/falcon-sensor start

tail -f /var/log/syslog | grep falcon
